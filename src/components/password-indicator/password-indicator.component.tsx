import React, { FC } from 'react';
import styled from 'styled-components';
import zxcvbn from 'zxcvbn';

import { createPasswordLabel } from '../../utils/snippets';
import { colors } from '../../utils/colors';

const StrengthMeter = styled.div`
  text-align: center;
  width: 40rem;
  margin-bottom: 2rem;

  span {
    font-size: 1.4rem;
  }
`;

const Progress = styled.progress`
  -webkit-appearance: none;
  appearance: none;
  height: 8px;
  width: 100%;

  &::-webkit-progress-bar {
    background-color: #eee;
    border-radius: 3px;
  }

  &::-webkit-progress-value {
    border-radius: 2px;
    background-size: 35px 20px, 100% 100%, 100% 100%;
  }

  &.strength-Weak::-webkit-progress-value {
    background-color: ${colors.weak};
  }
  &.strength-Fair::-webkit-progress-value {
    background-color: ${colors.fair};
  }
  &.strength-Good::-webkit-progress-value {
    background-color: ${colors.good};
  }
  &.strength-Strong::-webkit-progress-value {
    background-color: ${colors.strong};
  }
`;

const PasswordIndicator: FC<{ strength: string }> = ({ strength }) => {
  if (!strength) {
    return null;
  }

  const passwordStrength = zxcvbn(strength);

  return (
    <StrengthMeter>
      <Progress
        className={`strength-${createPasswordLabel(passwordStrength.score)}`}
        value={passwordStrength.score}
        max="4"
      />
      <br />

      <span className="password-strength-meter-label">
        {strength && (
          <>
            <strong>Password strength:</strong>{' '}
            {createPasswordLabel(passwordStrength.score)}
          </>
        )}
      </span>
    </StrengthMeter>
  );
};

export default PasswordIndicator;
