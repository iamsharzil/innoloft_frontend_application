import React, { FC } from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';

type Props = { error?: string | undefined };

const Style = styled.div`
  background: ${colors.red};
  color: #fff;
  text-align: center;
  font-weight: bold;
  padding: 1rem 0;
  margin-bottom: 2rem;
  font-size: 1.6rem;
  border-radius: 5px;
  width: 100%;
`;

const ErrorMessage: FC<Props> = ({ error }) => {
  if (!error) {
    return null;
  }

  return <Style>{error}</Style>;
};

export default ErrorMessage;
