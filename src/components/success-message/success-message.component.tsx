import React, { FC } from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';

const Style = styled.div`
  font-size: 2rem;
  font-weight: bold;
  color: ${colors.strong};
  margin-top: 1rem;
`;

const SuccessMessage: FC<{ message: string }> = ({ message }) => {
  return <Style>{message}</Style>;
};

export default SuccessMessage;
