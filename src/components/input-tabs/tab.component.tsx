import React, { FC } from 'react';
import styled from 'styled-components';
import cn from 'classnames';

type Props = {
  handleToggleForm: (id: number) => void;
  text: string;
  activeTab: boolean;
  id: number;
};

const Wrapper = styled.button`
  background-color: #eee;
  color: #000;

  cursor: pointer;
  flex: 1;
  padding: 2rem;
  outline: none;
  border: none;

  &.active {
    font-weight: bold;
    border-bottom: 2px solid;
  }

  :nth-child(1) {
    border-right: 2px solid #000;
  }
`;

const Tab: FC<Props> = ({ text, handleToggleForm, activeTab, id }) => {
  return (
    <Wrapper
      type="button"
      onClick={() => handleToggleForm(id)}
      className={cn({ active: activeTab })}
    >
      {text}
    </Wrapper>
  );
};

export default Tab;
