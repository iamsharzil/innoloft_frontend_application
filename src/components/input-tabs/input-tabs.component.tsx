import React, { FC } from 'react';

import styled from 'styled-components';
import Tab from './tab.component';

const Wrapper = styled.div`
  display: flex;

  text-align: center;
`;

type Props = {
  handleToggleForm: (id: number) => void;
  activeTab: number;
};

const InputTabs: FC<Props> = ({ handleToggleForm, activeTab }) => {
  return (
    <Wrapper>
      <Tab
        text="Account Settings"
        handleToggleForm={handleToggleForm}
        id={1}
        activeTab={activeTab === 1}
      />
      <Tab
        text="User Information"
        handleToggleForm={handleToggleForm}
        id={2}
        activeTab={activeTab === 2}
      />
    </Wrapper>
  );
};

export default InputTabs;
