/* eslint-disable @typescript-eslint/indent */
import React, { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import styled from 'styled-components';

import Button from '../forms/button.component';
import Input from '../forms/input.component';
import ErrorMessage from '../error-message/error-message.component';
import PasswordIndicator from '../password-indicator/password-indicator.component';
import SuccessMessage from '../success-message/success-message.component';

import {
  checkUppercaseCharacter,
  checkLowercaseCharacter,
  checkSpecialCharacter,
  checkNumber,
} from '../../utils/snippets';

const Form = styled.form`
  width: 40rem;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

type Inputs = {
  email: string;
  password: string;
  confirmPassword: string;
};

const AccountSettings = () => {
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  const { register, handleSubmit, reset, errors, getValues, watch } = useForm<
    Inputs
  >({
    mode: 'onChange',
  });

  const handleOnSubmit: SubmitHandler<Inputs> = (data) => {
    setLoading(true);

    setTimeout(() => {
      setLoading(false);
      if (Object.keys(data).length) {
        // eslint-disable-next-line no-alert
        alert(JSON.stringify(data));
        setSuccess(true);
        reset();
      }
    }, 2000);
  };

  return (
    <>
      <Form onSubmit={handleSubmit(handleOnSubmit)}>
        <Wrapper>
          <Input
            type="email"
            name="email"
            placeHolder="Email Address"
            icon="/icons/email.svg"
            ref={register({
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: 'Please enter a valid email address',
              },
            })}
          />
          <ErrorMessage error={errors.email?.message} />
        </Wrapper>

        <Wrapper>
          <Input
            type="password"
            name="password"
            placeHolder="Password"
            icon="/icons/lock.svg"
            ref={register({
              minLength: {
                value: 8,
                message: 'Password should contain atleast 8 characters',
              },
              validate: {
                isUpper: (value: string) =>
                  checkUppercaseCharacter(value) ||
                  'Password should contain atleast one uppercase character',

                isLower: (value: string) =>
                  checkLowercaseCharacter(value) ||
                  'Password should contain atleast one lowercase character',

                isSpecial: (value: string) =>
                  checkSpecialCharacter(value) ||
                  'Password should contain atleast one special character',

                isNumber: (value: string) =>
                  checkNumber(value) ||
                  'Password should contain atleast one number',
              },
            })}
          />
          <ErrorMessage error={errors.password?.message} />
        </Wrapper>

        <Wrapper>
          <Input
            type="password"
            name="confirmPassword"
            placeHolder="Confirm Password"
            icon="/icons/lock.svg"
            ref={register({
              validate: (value) =>
                value === getValues('password') || 'Passwords do not match',
            })}
          />
          <ErrorMessage error={errors.confirmPassword?.message} />
        </Wrapper>

        <PasswordIndicator strength={watch('password')} />

        <Button
          disabled={!!Object.keys(errors).length || loading}
          name={loading ? 'UPDATING' : 'UPDATE'}
        />
      </Form>

      {success && (
        <SuccessMessage message="Information updated succesfully!!" />
      )}
    </>
  );
};

export default AccountSettings;
