import React from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';

const Wrapper = styled.div`
  display: flex;
  align-items: stretch;
  width: 100%;

  color: ${colors.textColor};

  border: 2px solid ${colors.grey3};
  border-radius: 5px;

  margin-bottom: 2rem;

  span {
    background-color: ${colors.grey4};
    padding: 1rem;
    border-right: 2px solid ${colors.grey3};
  }

  input {
    border: none;
    outline: none;
    flex: 1;
    padding-left: 1rem;
  }
`;

type Props = {
  type: string;
  placeHolder: string;
  icon: string;
  name: string;
  ref: React.RefObject<HTMLInputElement>;
};

/**
 * TODO
 * CHANGE IMG TO SVG
 */

const Input = React.forwardRef<HTMLInputElement, Props>(
  ({ type, name, placeHolder, icon }, ref) => {
    return (
      <Wrapper>
        <span>
          <img src={icon} width={16} alt="#" />
        </span>
        <input name={name} type={type} placeholder={placeHolder} ref={ref} />
      </Wrapper>
    );
  },
);

export default Input;
