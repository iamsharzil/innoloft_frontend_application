import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';

type Props = {
  type?: string;
  placeHolder?: string;
  icon?: string;
  name?: string;
  ref: React.RefObject<HTMLSelectElement>;
  children: ReactNode;
};

const Style = styled.select`
  width: 100%;
  color: ${colors.textColor};
  border: 2px solid ${colors.grey3};
  background: #fff;
  padding: 1rem;
  border-radius: 5px;
  outline: none;
  margin-bottom: 2rem;
`;

const Select = React.forwardRef<HTMLSelectElement, Props>(
  ({ name, children }, ref) => {
    return (
      <Style name={name} ref={ref}>
        {children}
      </Style>
    );
  },
);

export default Select;
