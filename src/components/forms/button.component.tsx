import React, { FC } from 'react';
import styled from 'styled-components';
import cn from 'classnames';

import { colors } from '../../utils/colors';

const StyledButton = styled.button`
  border: none;
  padding: 1rem;
  color: #fff;
  width: 40rem;
  border-radius: 5px;

  background-color: ${({ disabled }) =>
    disabled ? '#bfbfbf' : colors.primaryColor};
  color: ${({ disabled }) => (disabled ? '#212529' : '#fff')};
  cursor: ${({ disabled }) => disabled && 'not-allowed'};
`;

const Button: FC<{ name: string; disabled: boolean }> = ({
  name,
  disabled,
}) => {
  return (
    <StyledButton
      disabled={disabled}
      type="submit"
      className={cn({
        disabled,
      })}
    >
      {name}
    </StyledButton>
  );
};

export default Button;
