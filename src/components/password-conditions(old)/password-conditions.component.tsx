import React, { FC } from 'react';
import { FieldError } from 'react-hook-form';
import styled from 'styled-components';

import { colors } from '../../utils/colors';

const Styles = styled.div`
  margin-bottom: 2rem;

  span {
    font-weight: bold;
    font-size: 1.8rem;
  }

  ul {
    font-size: 1.6rem;
    margin-left: 5rem;

    li {
      color: ${colors.red};

      &.active {
        color: ${colors.green};
      }
    }
  }
`;

const PasswordConditions: FC<{ password?: FieldError | undefined }> = ({
  password,
}) => {
  return (
    <Styles>
      {/* <span>Password must have: </span>
      <ul>
        <li
          className={cn({
            active: password?.type === 'isNumber',
          })}
        >
          atleast 1 number
        </li>
        <li
          className={cn({
            active: password?.type === 'isLower',
          })}
        >
          atleast 1 smallcase character
        </li>
        <li
          className={cn({
            active: !(password?.type === 'isUpper'),
          })}
        >
          atleast 1 uppercase character
        </li>
        <li
          className={cn({
            active: !(password?.type === 'isSpecial'),
          })}
        >
          atleast 1 special character
        </li>
      </ul> */}
    </Styles>
  );
};

export default PasswordConditions;
