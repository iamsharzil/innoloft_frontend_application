import React, { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import styled from 'styled-components';

import Button from '../forms/button.component';
import Input from '../forms/input.component';
import ErrorMessage from '../error-message/error-message.component';
import SuccessMessage from '../success-message/success-message.component';
import Select from '../forms/select.component';

const Form = styled.form`
  width: 40rem;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

type Inputs = {
  firstName: string;
  lastName: string;
  street: string;
  houseNo: string;
  postal: number;
  country: {};
};

const UserInformation = () => {
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  const { register, handleSubmit, reset, errors } = useForm<Inputs>({
    mode: 'onChange',
  });

  const handleOnSubmit: SubmitHandler<Inputs> = (data) => {
    setLoading(true);

    setTimeout(() => {
      setLoading(false);
      if (Object.keys(data).length) {
        // eslint-disable-next-line no-alert
        alert(JSON.stringify(data));
        setSuccess(true);
        reset();
      }
    }, 2000);
  };

  return (
    <>
      <Form onSubmit={handleSubmit(handleOnSubmit)}>
        <Wrapper>
          <Input
            type="text"
            name="firstName"
            placeHolder="First name"
            icon="/icons/user.svg"
            ref={register({
              required: {
                value: true,
                message: 'Please enter your first name',
              },
            })}
          />
          <ErrorMessage error={errors.firstName?.message} />
        </Wrapper>

        <Wrapper>
          <Input
            type="text"
            name="lastName"
            placeHolder="Last name"
            icon="/icons/user.svg"
            ref={register({
              required: {
                value: true,
                message: 'Please enter your last name',
              },
            })}
          />
          <ErrorMessage error={errors.lastName?.message} />
        </Wrapper>

        <Wrapper>
          <Input
            type="text"
            name="street"
            placeHolder="Street"
            icon="/icons/home.svg"
            ref={register({
              required: {
                value: true,
                message: 'Please enter the street name',
              },
            })}
          />
          <ErrorMessage error={errors.street?.message} />
        </Wrapper>

        <Wrapper>
          <Input
            type="text"
            name="houseNo"
            placeHolder="House No"
            icon="/icons/home.svg"
            ref={register({
              required: {
                value: true,
                message: 'Please enter the house no.',
              },
            })}
          />
          <ErrorMessage error={errors.houseNo?.message} />
        </Wrapper>

        <Wrapper>
          <Input
            type="text"
            name="postal"
            placeHolder="Postal Code"
            icon="/icons/home.svg"
            ref={register({
              required: {
                value: true,
                message: 'Please enter the postal code',
              },
            })}
          />
          <ErrorMessage error={errors.postal?.message} />
        </Wrapper>

        <Wrapper>
          <Select name="country" ref={register}>
            <option value="GM">Germany</option>
            <option value="AU">Austria</option>
            <option value="SW">Switzerland</option>
          </Select>
        </Wrapper>

        <Button
          disabled={!!Object.keys(errors).length || loading}
          name={loading ? 'UPDATING' : 'UPDATE'}
        />
      </Form>

      {success && (
        <SuccessMessage message="Information updated succesfully!!" />
      )}
    </>
  );
};

export default UserInformation;
