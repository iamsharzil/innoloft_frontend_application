import React from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';

const Logo = () => <img width={140} src="/icons/logo.svg" alt="Logo" />;

const Wrapper = styled.header`
  background-color: ${colors.primaryColor};
  color: #fff;
  height: 10vh;
  display: flex;
  align-items: center;

  img {
    margin-left: 2rem;
  }
`;

const Header = () => {
  return (
    <Wrapper>
      <Logo />
    </Wrapper>
  );
};

export default Header;
