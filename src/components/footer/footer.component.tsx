import React from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';

const Wrapper = styled.footer`
  background-color: ${colors.grey1};
  color: #fff;
  height: 5vh;
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const Footer = () => {
  return <Wrapper>©2020 Innoloft GmbH</Wrapper>;
};

export default Footer;
