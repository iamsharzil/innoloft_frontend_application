import React from 'react';
import styled from 'styled-components';
import { sizes } from '../../utils/breakpoints';

/**
 * TODO
 * MAKE DYNAMIC HEIGHT FOR ASIDE
 */

const Wrapper = styled.aside`
  border-right: 2px solid;
  width: 20%;
  height: calc(100vh - 15vh);

  @media ${sizes.small} {
    width: 100%;
    height: auto;
    border-right: 0;
    border-bottom: 2px solid;
  }

  ul {
    font-size: 1.6rem;
    width: 50%;
    margin: 0 auto;
    margin-top: 5rem;
    list-style: none;

    @media ${sizes.small} {
      font-size: 1.4rem;
      margin-top: 2rem;
      margin-left: 2rem;
      margin-bottom: 2rem;
    }

    li {
      padding: 1rem 0;
      cursor: pointer;

      &:hover {
        font-weight: bold;
      }
    }
  }
`;

const Aside = () => {
  return (
    <Wrapper>
      <ul>
        <li>Home</li>
        <li>Members</li>
        <li>Marketplace</li>
        <li>Matching</li>
        <li>News</li>
        <li>Startups</li>
        <li>Companies</li>
        <li>Ecosystem</li>
        <li>Events</li>
        <li>Tools</li>
      </ul>
    </Wrapper>
  );
};

export default Aside;
