import React from 'react';
import styled from 'styled-components';
import { sizes } from '../../utils/breakpoints';
import Aside from '../aside/aside.component';
import UserForms from '../user-forms/user-info.component';

const Wrapper = styled.main`
  display: flex;

  @media ${sizes.small} {
    flex-direction: column;
  }
`;

const Main = () => {
  return (
    <Wrapper>
      <Aside />
      <UserForms />
    </Wrapper>
  );
};

export default Main;
