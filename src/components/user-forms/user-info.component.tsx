import React, { useState } from 'react';
import styled from 'styled-components';
import { sizes } from '../../utils/breakpoints';
import AccountSettings from '../account-settings/account-settings.component';
import InputTabs from '../input-tabs/input-tabs.component';
import UserInformation from '../user-information/user-information.component';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 80%;

  @media ${sizes.small} {
    width: 100%;
  }
`;

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  margin: 10rem 0;
`;

const UserForms = () => {
  const [toggleForm, setToggleForm] = useState(1);

  const handleToggleForm = (id: number) => setToggleForm(id);

  return (
    <Wrapper>
      <InputTabs activeTab={toggleForm} handleToggleForm={handleToggleForm} />

      <FormWrapper>
        {toggleForm === 1 && <AccountSettings />}
        {toggleForm === 2 && <UserInformation />}
      </FormWrapper>
    </Wrapper>
  );
};

export default UserForms;
