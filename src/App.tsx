import React from 'react';
import Footer from './components/footer/footer.component';
import Header from './components/header/header.component';
import Main from './components/main/main.component';

function App() {
  return (
    <>
      <Header />
      <Main />
      <Footer />
    </>
  );
}

export default App;
