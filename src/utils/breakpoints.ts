export const breakpoints = {
  mobile: 480, // 30em
  mobileL: 768, // 48em
  tablet: 1024, // 64em
};

export const sizes = {
  small: `(max-width: ${breakpoints.mobile / 16}em)`,
  medium: `(max-width: ${breakpoints.tablet / 16}em)`,
};
