export const colors = {
  primaryColor: '#273271',
  textColor: '#454D4B',

  grey1: '#445063',
  grey2: '#f6f6f6',
  grey3: '#6c757d',
  grey4: '#e9ecef',

  red: '#e40808',
  green: '#28a745',

  weak: '#f25f5c',
  fair: '#ffe066',
  good: '#247ba0',
  strong: '#70c1b3',
};
