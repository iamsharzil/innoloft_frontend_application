export const checkUppercaseCharacter = (value: string) => {
  const status = /[A-Z]/.test(value);
  return status;
};

export const checkLowercaseCharacter = (value: string) => {
  const status = /[a-z]/.test(value);
  return status;
};

export const checkNumber = (value: string) => {
  const status = /[0-9]/.test(value);
  return status;
};

export const checkSpecialCharacter = (value: string) => {
  const status = /[!@#$%^&+=]/.test(value);
  return status;
};

// export const getPasswordStrength = (strength: string) => {
//   if (
//     strength.length > 0 &&
//     (checkLowercaseCharacter(strength) ||
//       checkUppercaseCharacter(strength) ||
//       checkNumber(strength) ||
//       checkSpecialCharacter(strength))
//   ) {
//     return {
//       message: 'WEAK',
//       state: true,
//     };
//   }

//   if (
//     strength.length >= 7 &&
//     strength.length <= 10 &&
//     ((checkLowercaseCharacter(strength) && checkUppercaseCharacter(strength)) ||
//       (checkLowercaseCharacter(strength) && checkNumber(strength)) ||
//       (checkUppercaseCharacter(strength) && checkNumber(strength)) ||
//       (checkSpecialCharacter(strength) && checkNumber(strength)) ||
//       (checkSpecialCharacter(strength) && checkLowercaseCharacter(strength)) ||
//       (checkSpecialCharacter(strength) && checkUppercaseCharacter(strength)))
//   ) {
//     return {
//       message: 'MEDIUM',
//       state: true,
//     };
//   }

//   if (
//     strength.length >= 11 &&
//     strength.length <= 14 &&
//     ((checkLowercaseCharacter(strength) &&
//       checkUppercaseCharacter(strength) &&
//       checkNumber(strength)) ||
//       (checkLowercaseCharacter(strength) &&
//         checkUppercaseCharacter(strength) &&
//         checkSpecialCharacter(strength)) ||
//       (checkUppercaseCharacter(strength) &&
//         checkNumber(strength) &&
//         checkSpecialCharacter(strength)) ||
//       (checkLowercaseCharacter(strength) &&
//         checkNumber(strength) &&
//         checkSpecialCharacter(strength)))
//   ) {
//     return {
//       message: 'STRONG',
//       state: true,
//     };
//   }

//   if (
//     strength.length > 14 &&
//     ((checkLowercaseCharacter(strength) &&
//       checkUppercaseCharacter(strength) &&
//       checkNumber(strength)) ||
//       (checkLowercaseCharacter(strength) &&
//         checkUppercaseCharacter(strength) &&
//         checkSpecialCharacter(strength)) ||
//       (checkUppercaseCharacter(strength) &&
//         checkNumber(strength) &&
//         checkSpecialCharacter(strength)) ||
//       (checkLowercaseCharacter(strength) &&
//         checkNumber(strength) &&
//         checkSpecialCharacter(strength)))
//   ) {
//     return {
//       message: 'VERY STRONG',
//       state: true,
//     };
//   }
// };

export const createPasswordLabel = (result: number) => {
  switch (result) {
    case 0:
      return 'Weak';
    case 1:
      return 'Weak';
    case 2:
      return 'Fair';
    case 3:
      return 'Good';
    case 4:
      return 'Strong';
    default:
      return 'Weak';
  }
};
